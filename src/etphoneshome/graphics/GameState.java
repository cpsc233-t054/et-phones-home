package etphoneshome.graphics;

/**
 * Values of the state of the game used to track where in the game the player is viewing
 */
public enum GameState {

    MAIN_MENU(),
    INSTRUCTIONS_MENU(),
    HIGHSCORES_MENU(),
    PAUSE_MENU(),
    END_GAME(),
    IN_GAME();

}
