package etphoneshome.objects;

/**
 * The given Direction constants used to track which direction a player is facing or the relationship between a chracter's hitbox and other hitboxes (mainly platforms)
 */
public enum Direction {

    NORTH(),
    EAST(),
    SOUTH(),
    WEST(),

    ABOVE(),
    LEFT_OF(),
    RIGHT_OF(),
    BELOW();

}
